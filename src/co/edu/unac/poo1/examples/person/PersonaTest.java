package co.edu.unac.poo1.examples.person;

import java.util.Scanner;

public class PersonaTest {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        Persona person1 = new Persona();

        System.out.println("Ingrese el nombre de la persona 1");
        String name = input.nextLine();
        person1.setNombre(name);
        System.out.println("El nombre de la persona es "
                + person1.getNombre());

        Persona person2 = new Persona("Alison");
        System.out.println("El nombre de la persona es "
                + person2.getNombre());

        System.out.println(person1.toString());
        System.out.println(person2.toString());
    }
}
