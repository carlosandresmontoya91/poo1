package co.edu.unac.poo1.fundamentals;

import javax.swing.*;
import java.util.Scanner;

public class ShowMessages {
    public static void main(String[] args) {
        System.out.println("Hello!");
        JOptionPane.showMessageDialog(null, "Hello!");

        Scanner sc = new Scanner(System.in);
        System.out.println("Give me your name");
        String nameConsole = sc.nextLine();
        System.out.println(nameConsole);

        String nameJOption = JOptionPane.showInputDialog("Give me your name");
        JOptionPane.showMessageDialog(null, nameJOption);

    }
}
