package co.edu.unac.poo1.fundamentals;

public class LinealizationExercises {

    public static void main(String args[]) {

        float a = 4;
        float b = 2;
        float c = 5;
        float d = 1;
        float exercise_1 = (a + (b / c)) / ((a / b) + c);
        float exercise_2 = (a + b + (a / b)) / c;
        float exercise_3 = (a / (a + b)) / (a / (a - b));
        float exercise_4 = (a + (a / (a + b + (b / c)))) / (a + (b / (c + a)));
        float exercise_5 = (a + b + c) / (a + (b / c));
        float exercise_6 = (a + b + (c / (d * a))) / (a + b * (c / d));
        float exercise_7 = (a + (b / c) + d) / a;
        float exercise_8 = ((a / b) + (b / c)) / ((a / b) - (b / c));
        float exercise_9 = a + ((a + ((a + b) / (c + d))) / (a + (a / b)));
        float exercise_10 = a + b + (c / d) + ((a / (b - c)) / (a / (b + c)));

        System.out.println("a = 4\nb = 2\nc = 5\nd = 1\nEjercicio 1 " + exercise_1 + "\nEjercicio 2 " + exercise_2 + "\nEjercicio 3 " + exercise_3 +
                "\nEjercicio 4 " + exercise_4 + "\nEjercicio 5 " + exercise_5 + "\nEjercicio 6 " + exercise_6 + "\nEjercicio 7 "
                + exercise_7 + "\nEjercicio 8 " + exercise_8 + "\nEjercicio 9 " + exercise_9 + "\nEjercicio 10 " + exercise_10);

    }

}
