package co.edu.unac.poo1.composition;

import java.util.ArrayList;

public class MainTest {
    public static void main(String[] args) {
        Book book1 = new Book("Mastering Java 11", "Edward Lavieri");
        Book book2 = new Book("Java how to program", "Paul y Harvey Deitel");
        ArrayList<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        Library library1 = new Library(books);
        System.out.println(library1.toString());

    }
}
