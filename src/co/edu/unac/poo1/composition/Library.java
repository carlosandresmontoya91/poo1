package co.edu.unac.poo1.composition;

import java.util.ArrayList;

public class Library {

    private ArrayList<Book> books;

    public Library() {

    }
    public Library(ArrayList<Book> books) {
        this.books = books;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Library{" +
                "books=" + books +
                '}';
    }
}
