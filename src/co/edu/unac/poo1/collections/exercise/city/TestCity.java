package co.edu.unac.poo1.collections.exercise.city;

import java.util.ArrayList;
import java.util.Scanner;

public class TestCity {

    // Variables globales
    static Scanner sc = new Scanner(System.in);
    static ArrayList<Ciudad> ciudades = new ArrayList<Ciudad>();
    static String[] diasSemana = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"};

    public static void main(String[] args) {

        // Arreglo auxiliar de días de la semana


        /*Ciudad ciudad1 = new Ciudad();
        ciudad1.setNombre("Medellín");
        ciudad1.setPais("Colombia");
        ciudad1.setTemperatura(new double[] {26, 28, 24.5, 25, 28.6, 27, 23.4});
        System.out.println(ciudad1.toString());
        System.out.println("El día más caluroso fue el " + diasSemana[ciudad1.diaMasCaluroso()]);
        System.out.println("El día más frío fue el " + diasSemana[ciudad1.diaMasFrio()]);
        System.out.println("El promedio de temperatura fue de " + ciudad1.promedioTemperatura());
         */
        int opcion = -1;
        System.out.println("Bienvenido");
        while (opcion != 0) {
            System.out.println("-----------------------------------------------------------------\n" +
                    "Selecciona la opción que deseas realizar\n1. Crear una ciudad con toda su información" +
                    "\n2. Obtener información de una ciudad creada\n3. Ver toda la información\n0. Salir\n" +
                    "-----------------------------------------------------------------\n");
            opcion = Integer.parseInt(sc.nextLine());

            // Switch: Estructura condicional
            switch (opcion) {
                case 1: // if
                    crearCiudad();
                    break;
                case 2: // if
                    buscarUnaCiudad();
                    break;
                case 3:
                    listarCiudades();
                    break;
                case 0:
                    System.out.println("El programa se cerrará en breve");
                    System.exit(0);
                default: // else
                    System.out.println("La opción que ingresaste no es válida");
                    break;
            }

        }

    }

    public static void crearCiudad() {

        Ciudad ciudad = new Ciudad();
        System.out.println("Crear una nueva ciudad");
        System.out.println("Ingrese el nombre de la ciudad");
        ciudad.setNombre(sc.nextLine());
        System.out.println("Ingrese el país de la ciudad");
        ciudad.setPais(sc.nextLine());
        System.out.println("Temperatura de la ciudad en los últimos 7 días");
        double[] temperatura7Dias = new double[7];
        for (int i = 0; i < 7; i++) {
            System.out.println("Ingrese la temperatura del día " + diasSemana[i]);
            temperatura7Dias[i] = Double.parseDouble(sc.nextLine());
        }
        ciudad.setTemperatura(temperatura7Dias);
        ciudades.add(ciudad);
    }

    public static void buscarUnaCiudad() {
        System.out.println("Estas son las ciudades creadas");
        for (int i = 0; i < ciudades.size(); i++) {
            System.out.println(ciudades.get(i).getNombre());
        }
        System.out.println("Ingresa el nombre de la ciudad de la que deseas ver información");
        String nombreCiudad = sc.nextLine(); // Medellín
        String verTemperatura = "Y";
        for (int i = 0; i < ciudades.size(); i++) {                 // objeto1 === objeto2
            if (ciudades.get(i).getNombre().equals(nombreCiudad)) { // objeto1.equals(objeto2)
                System.out.println(ciudades.get(i).toString());
                System.out.println("El día más caluroso fue el " + diasSemana[ciudades.get(i).diaMasCaluroso()]); // diasSemana[0]
                System.out.println("El día más frío fue el " + diasSemana[ciudades.get(i).diaMasFrio()]);
                System.out.println("El promedio de temperatura en los últimos 3 días fue de "
                        + ciudades.get(i).promedioTemperatura());
                while (verTemperatura.equals("Y")) {
                    System.out.println("¿Deseas ver la temperatura de un día en específico?\nY -> Sí\nN -> No");
                    verTemperatura = sc.nextLine();
                    if (verTemperatura.equals("Y")) {
                        System.out.println("Ingresa el número del día (1-7) del que deseas ver la temperatura\nTen en " +
                                "cuenta que 1 = Domingo y 7 = Sábado");
                        int dia = Integer.parseInt(sc.nextLine());
                        System.out.println("La temperatura del día " + diasSemana[dia-1] + " fue de " + ciudades.get(i).verTemperaturaDeUnDia(dia-1));

                    }
                }
            }
        }
    }

    public static void listarCiudades() {
        System.out.println("Lista de ciudades creadas");
        for (int i = 0; i < ciudades.size(); i++) {
            System.out.println(ciudades.get(i).toString());
        }
    }

}
