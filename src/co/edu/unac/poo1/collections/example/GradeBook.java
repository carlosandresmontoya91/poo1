package co.edu.unac.poo1.collections.example;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;

public class GradeBook {
    private String courseName;
    private int[] grades;

    // Constructor por defecto (no tiene parámetros, es un constructor vacío)
    // Con este constructor puedo crear objetos vacíos
    public GradeBook() {

    }

    // Constructor con todos los parámetros
    public GradeBook(String courseName, int[] grades) {
        this.courseName = courseName;
        this.grades = grades;
    }

    // Getters y setters
    // Obtener los atributos | Asignar un dato a mi atributo
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    // Obtener el promedio de las notas
    public String getAverage() {
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", symbols);
        double total = 0;
        double average = 0;
        for (int grade : grades)
            total += grade;
        average = total / grades.length;
        return df.format(average); // return (double) total / grades.length;
    }

    // Obtener la nota máxima
    public int getMaximum() {
        int highGrade = grades[0]; // Definir la primera nota como la más alta
        for (int grade : grades) {
            if (grade > highGrade)
                highGrade = grade; // Si el elemento actual es mayor que highGrade, highGrade se define ahora como ese elemento
        }
        return highGrade;
    }

    // Obtener la nota más baja
    public int getMinimum() {
        int lowGrade = grades[0]; // Definir la primera nota como la más baja
        for (int grade : grades) {
            if (grade < lowGrade)
                lowGrade = grade; // Si el elemento actual es menor que lowGrade, lowGrade se define ahora como ese elemento
        }
        return lowGrade;
    }

    // Imprimir la información del objeto
    @Override
    public String toString() {
        return "GradeBook { " +
                "courseName='" + courseName + '\'' +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}
