package co.edu.unac.poo1.collections.example;

import java.util.Scanner;

public class MainGradeBook {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        GradeBook gradeBook = new GradeBook();
        gradeBook.setCourseName("POO1");
        int[] grades = new int[3];
        System.out.println("Ingrese la primera nota");
        grades[0] = Integer.parseInt(sc.nextLine());
        System.out.println("Ingrese la segunda nota");
        grades[1] = Integer.parseInt(sc.nextLine());
        System.out.println("Ingrese la tercer nota");
        grades[2] = Integer.parseInt(sc.nextLine());
        //gradeBook.setGrades(new int[] {5, 5, 5, 3, 4, 2, 1});
        gradeBook.setGrades(grades);
        System.out.println(gradeBook.toString());
        System.out.println("Promedio = " + gradeBook.getAverage());
        System.out.println("Máxima nota = " + gradeBook.getMaximum());
        System.out.println("Nota más baja = " + gradeBook.getMinimum());
    }
}
