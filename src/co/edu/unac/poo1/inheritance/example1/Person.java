package co.edu.unac.poo1.inheritance.example1;

public class Person {

    //Atributos

    private String firstName;

    private String lastName;

    private int age;

    //Constructor

    //Constructor por defecto

    //Constructor que no tiene parámetros
    //Crea un objeto vacío
    public Person() {

    }

    //Constructor con todos los parámetros
    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    //Getters y setters

    //get()
    //El tipo de dato de retorno del atributo del que estoy hablando: public String
    //Dentro del método retorna el atributo: return firstName;
    public String getFirstName() {
        return firstName;
    }

    //set()
    //public void porque no retorna nada
    //Dentro del método uso el this.atributo para actualizar el valor
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //Otros métodos

    //toString() me permite ver la información de los objetos creados
    @Override
    public String toString() {
        return "Persona -> " +
                "nombre: '" + firstName + '\'' +
                ", apellido: '" + lastName + '\'' +
                ", edad: " + age + " y " + olderThan18();
    }

    //Método para calcular si es mayor o menor de edad

    public String olderThan18() {
        if(age >= 18) {
            return "es mayor de 18 años";
        } else {
            return "es menor de 18 años";
        }
    }

}
