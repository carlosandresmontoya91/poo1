package co.edu.unac.poo1.inheritance.example2;

public class Pelicula extends ContenidoMultimedia {

    //Atributos únicos de la clase Pelicula
    private String fechaEstreno;

    //Constructor por defecto
    public Pelicula() {
    }

    //Constructor con todos los parámetros
    public Pelicula(String nombre, String tipo, int duracion, String genero, int cantidadVistas, String fechaEstreno) {
        super(nombre, tipo, duracion, genero, cantidadVistas);
        this.fechaEstreno = fechaEstreno;
    }

    //Métodos get y set

    public String getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(String fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    //Método toString() modificado para película
    @Override
    public String toString() {
        return "Pelicula{ nombre='" + getNombre() + '\'' +
                ", tipo='" + getTipo() + '\'' +
                ", duracion=" + getDuracion() +
                ", genero='" + getGenero() + '\'' +
                ", cantidadVistas=" + getCantidadVistas()+
                "fechaEstreno='" + fechaEstreno + '\'' +
                '}';
    }
}
