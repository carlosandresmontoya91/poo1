package co.edu.unac.poo1.inheritance.example2;

public class CortoAnimado extends ContenidoMultimedia{

    //Atributos únicos de la clase Documental
    private boolean es3D;

    //Constructor por defecto
    public CortoAnimado() {
    }

    //Constructor con todos los parámetros
    public CortoAnimado(String nombre, String tipo, int duracion, String genero, int cantidadVistas, boolean es3D) {
        super(nombre, tipo, duracion, genero, cantidadVistas);
        this.es3D = es3D;
    }

    //Métodos get y set

    public boolean isEs3D() {
        return es3D;
    }

    public void setEs3D(boolean es3D) {
        this.es3D = es3D;
    }

    //Método toString() modificado para película
    @Override
    public String toString() {
        return "CortoAnimado{ nombre='" + getNombre() + '\'' +
                ", tipo='" + getTipo() + '\'' +
                ", duracion=" + getDuracion() +
                ", genero='" + getGenero() + '\'' +
                ", cantidadVistas=" + getCantidadVistas()+
                "es 3D='" + es3D + '\'' +
                '}';
    }
}
