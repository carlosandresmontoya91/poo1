package co.edu.unac.poo1.agregation;

public class Student {

    private String name;
    private int id ;
    private String faculty;

    public Student() {

    }

    public Student(String name, int id, String faculty)
    {

        this.name = name;
        this.id = id;
        this.faculty = faculty;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", faculty='" + faculty + '\'' +
                '}';
    }
}
